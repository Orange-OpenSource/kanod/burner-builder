#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#  
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

basedir=$(dirname "${BASH_SOURCE[0]}")

BURNER_BUILD=${BURNER_BUILD:-"true"}

export PATH="$HOME/.local/bin:$PATH"

url=${REPO_URL}
if [ -z "${url}" ]; then
  echo "No Nexus repository defined (fail)"
  exit 1
fi

error=0

groupId="kanod"
groupPath=$(echo "$groupId" | tr '.' '/')

if [ "$BURNER_BUILD" == "true" ]; then
  echo "*** Burners ***"
  version="$BURNER_VERSION"
  if [ -n "$version" ]; then
    artifactId="burner"
    if ! curl --head --silent --output /dev/null --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.kernel"; then
      echo "Building kernel/ramdisk for burning image"
      "${basedir}/burner-build.sh" -o burner
      if [ -f burner.kernel ]; then
        # shellcheck disable=SC2086
        mvn ${MAVEN_CLI_OPTS} -B deploy:deploy-file  ${MAVEN_OPTS}  -DgroupId="${groupId}" -DartifactId="${artifactId}" \
          -Dversion="${version}" -Dtype=kernel -Dfile=burner.kernel \
          -DrepositoryId=kanod -Durl="${url}" -Dfiles=burner.initramfs \
          -Dtypes=initramfs -Dclassifiers=ramdisk
      else
          echo "Build for burner failed"
          error=1
      fi
    fi
  fi
fi

echo "*** IPA ***"
version="$IPA_VERSION"
if [ -n "$version" ]; then
  artifactId="ironic-python-agent"
  if ! curl --head --silent --output /dev/null --fail "${url}/${groupPath}/${artifactId}/${version}/${artifactId}-${version}.tar"; then
    echo "Building kernel/ramdisk for Ironic Python Agent"
    # "${basedir}/ipa-build.sh"
    curl https://images.rdoproject.org/centos8/master/rdo_trunk/current-tripleo/ironic-python-agent.tar --output ${artifactId}.tar
    if [ -f ${artifactId}.tar ]; then
      tar xvf ${artifactId}.tar
      # shellcheck disable=SC2086
      mvn ${MAVEN_CLI_OPTS} -B deploy:deploy-file ${MAVEN_OPTS} -DgroupId="${groupId}" -DartifactId="${artifactId}" \
        -Dversion="${version}" -Dtype=tar -Dfile=${artifactId}.tar \
        -DrepositoryId=kanod -Durl="${url}" \
        -Dfiles="ironic-python-agent.initramfs,ironic-python-agent.kernel" \
        -Dtypes=initramfs,kernel -Dclassifiers=initramfs,kernel

    else
        echo "Build for Ironic Python Agent failed"
        error=1
    fi
  fi
fi

if [ "$error" == "1" ]; then
  exit 1
fi

