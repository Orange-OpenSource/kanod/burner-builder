#!/bin/bash

#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

debug=0
output=
key=

while getopts "dhk:o:?" opt; do
    case "$opt" in
    h|\?)
      echo "burner-build [-u|-c] -s <size>"
      echo "   -h help"
      echo "   -k <key>: public key for debug login"
      echo "   -d: debug"
      ;;
    d)
      debug=1
      ;;
    k)
      key=${OPTARG}
      ;;
    o)
      output=${OPTARG}
      ;;
    esac
done

if [ "$debug" == 1 ] ; then
    DEBUG_ELT=devuser
    export DIB_DEV_USER_PWDLESS_SUDO=yes
    export DIB_DEV_USER_AUTHORIZED_KEYS=${key:-${HOME}/.ssh/id_rsa.pub}
    export DIB_DEV_USER_PASSWORD=secret
    export DIB_DEV_USER_SHELL=/bin/bash
else
    DEBUG_ELT=''
fi

loc_ipab=$("${PYTHON:-/usr/bin/python3}" -m pip show ironic-python-agent-builder | grep -oP '(?<=^Location:).*' | xargs)
root_ipab=$(dirname "$(dirname "$(dirname "$loc_ipab")")")

BASEDIR=$(dirname "${BASH_SOURCE[0]}")

export ELEMENTS_PATH=$root_ipab/share/ironic-python-agent-builder/dib:$BASEDIR/elements

export DIB_RELEASE=focal
OUTPUT=${output:-ipa-burner}

# shellcheck disable=SC2086
disk-image-create -o "${OUTPUT}" ubuntu-minimal burn-image ${DEBUG_ELT}
