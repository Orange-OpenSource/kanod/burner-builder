#!/bin/bash

WORKDIR=${1:-LOCAL_TEST}

VM_IP=192.168.155.105
ISO_NAME=burner-vm.iso
VM_NAME=test-burner
STORAGE_POOL=burner-pool

export PATH="$HOME/.local/bin:$PATH"
echo "$PATH"

export no_proxy=${no_proxy},${VM_IP}

echo "Creating ISO"

cat > "${WORKDIR}/test-config/config.yaml" << EOF
name: ${VM_NAME}
boot:
  disk: vda

admin:
  passwd: 'secret'
  keys: []
network:
  version: 2
  ethernets:
    enp1s0:
      dhcp4: false
      addresses: [${VM_IP}/24]
      gateway4: 192.168.155.1
EOF

kanod-prepare -c  "${WORKDIR}"/test-config -o ${ISO_NAME}

state=$(virsh dominfo ${VM_NAME} 2> /dev/null)
if [ "$state" != "" ] ; then
  virsh destroy "${VM_NAME}"
  virsh undefine "${VM_NAME}"
fi

# shellcheck disable=SC2076
if [[ "$(virsh pool-list --name)" =~ "${STORAGE_POOL}" ]]; then
  for vol in $(virsh vol-list "${STORAGE_POOL}" | tail +3 | awk '{print $1}'); do
      virsh vol-delete --pool "${STORAGE_POOL}" --vol "${vol}";
  done
  virsh pool-destroy "${STORAGE_POOL}"
  virsh pool-undefine "${STORAGE_POOL}"
fi

mkdir -p "$(pwd)/burner-pool"
virsh pool-define-as --type dir --name "${STORAGE_POOL}" --target "$(pwd)/burner-pool"
virsh pool-start ${STORAGE_POOL}
virsh pool-autostart ${STORAGE_POOL}

virsh vol-create-as ${STORAGE_POOL} "${VM_NAME}"-disk 3G --format raw

virsh net-define /dev/stdin <<EOF
<network>
  <name>burner-net</name>
  <forward mode='nat'/>
  <bridge name='virbr5' stp='on' delay='0'/>
  <ip address='192.168.155.1' netmask='255.255.255.0'>
  </ip>
</network>
EOF
virsh net-start burner-net
virsh net-autostart burner-net


echo "Launching VM with ISO"

virt-install \
  --name "${VM_NAME}" \
  --memory 4096 \
  --vcpus 2 \
  --cdrom ./"${ISO_NAME}" \
  --disk "./${ISO_NAME},device=cdrom" \
  --disk "device=disk,vol=${STORAGE_POOL}/${VM_NAME}-disk,bus=virtio,format=raw" \
  --network "network=burner-net,model=virtio" \
  --os-variant ubuntu18.04 \
  --noautoconsole

echo
while true; do
  state=$(virsh dominfo ${VM_NAME} | grep -w "State:" | awk '{ print $2}')
  if [ "$state" != "" ] && [ "$state" == "running" ]; then
    echo -n "."
    sleep 10
  else
    break
  fi
done
echo

echo "Restarting VM"
virsh start ${VM_NAME}

echo -n 'wait for vm.'
count=0
while true; do
    # shellcheck disable=SC2034
    if status=$(ssh -i "${WORKDIR}/test-config/key" \
        -o "PasswordAuthentication=no" \
        -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
        "admin@${VM_IP}" cat /etc/kanod-configure/status 2> /dev/null)
    then
        break
    fi
    count=$((count+1))
    if [ "$count" -gt 200 ]; then
        echo 'Timout waiting for vm'
        exit 1
    fi
    sleep 10
    echo -n '.'
done

echo
virsh dominfo ${VM_NAME}
echo
echo "Result : "
ssh -i "${WORKDIR}/test-config/key" \
        -o "PasswordAuthentication=no" \
        -o "UserKnownHostsFile=/dev/null" -o "StrictHostKeyChecking=no" \
        "admin@${VM_IP}" cat /etc/kanod-configure/status 2> /dev/null

