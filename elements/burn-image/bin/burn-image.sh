#!/bin/bash

set -eu

echo "- mounting virtual disk"
while true; do
    if mount -t iso9660 /dev/sr0 /mnt -o ro; then
        break
    fi
    echo "- Waiting for sr0"
    sleep 5
done

DEVICE="/dev/$(grep -oP '(?<=^boot_disk:).*' /mnt/meta-data | xargs)"
DEBUG=$(grep -oP '(?<=^debug_burner:).*' /mnt/meta-data | xargs)
NO_AUTO_REBOOT=$(grep -oP '(?<=^no_auto_reboot:).*' /mnt/meta-data | xargs)
IMAGEFILE=/mnt/disk.qcow2

echo "- Checking boot device ${DEVICE} found in meta-data"
while true; do
    if test -b "${DEVICE}"; then
        break
    fi
    echo "- Waiting for ${DEVICE}"
    sleep 5
done


if ! [ -f "${IMAGEFILE}" ]; then
    echo "- no image file on virtual cd"
    exit 1
fi

echo "- Start burning with options:"
echo "  - DEBUG : ${DEBUG}"
echo "  - NO_AUTO_REBOOT : ${NO_AUTO_REBOOT}"
echo "- Writing image to disk"
echo "- Erasing existing GPT and MBR data structures from ${DEVICE}"

# NOTE(gfidente): GPT uses 33*512 sectors, this is an attempt to avoid bug:
# https://bugs.launchpad.net/ironic-python-agent/+bug/1737556
DEVICE_SECTORS_COUNT=$(blockdev --getsz "$DEVICE")
dd bs=512 if=/dev/zero "of=$DEVICE" count=33
dd bs=512 if=/dev/zero "of=$DEVICE" count=33 seek=$((DEVICE_SECTORS_COUNT - 33))
sgdisk -Z "$DEVICE"

echo "- Imaging $IMAGEFILE to $DEVICE"

# limit the memory usage for qemu-img to 1 GiB
ulimit -v 1048576
qemu-img convert -t directsync -O host_device "$IMAGEFILE" "$DEVICE"
sync

echo "-${DEVICE} imaged successfully!"

if [ "$DEBUG" == "1" ]; then
    echo "- Debug mode: no shutdown"
else
    if [ "$NO_AUTO_REBOOT" == "1" ]; then
        echo "- Power off now"
        shutdown now
    else
        echo "- Rebooting now"
        shutdown -r now
    fi
fi
